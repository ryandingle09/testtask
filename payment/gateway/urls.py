from django.urls import path
from . import views

app_name = 'gateway'
urlpatterns = [
	path('', views.OfferView.as_view(), name='offer'),
    path('identify', views.Identify.identify, name='identify'),
    path('preview', views.Preview.as_view(), name='preview'),
    path('confirm', views.AuthorizeItem.as_view(), name='confirm'),
]