from django.shortcuts import render
from django.views import View
from .helper import authorize
from django.http import HttpResponseRedirect
from django.http.request import HttpRequest
import json

payment_endpoint = 'http://127.0.0.1:55555'
app_endpoint = 'http://127.0.0.1:8000'

class OfferView(View):
    template = 'gateway/pages/offer.html'

    def get(self, request):
        data = {
            'test': 'test'
        }

        return render(request, self.template, data)

class Identify:
    def identify(self):
        data = {
            'command': 'wapIdentifyUser',
            'username': 'TESTUSER',
            'password': 'SeCrEt',
            'serviceCode': 'VAS0001',
            'userIp': '127.0.0.1',
            'callbackUrl': '%s/preview' % app_endpoint
        }

        auth            = authorize(url=payment_endpoint,data=data)
        response        = json.loads(auth.text)
        redirect_url    = response['redirectURL']

        return HttpResponseRedirect(redirect_url)

class Preview(View):
    template = 'gateway/pages/preview.html'

    def get(self, request):
        data = {
            'command': 'getUser',
            'username': 'TESTUSER',
            'password': 'SeCrEt',
            'serviceCode': 'VAS0001',
            'uid': request.GET['uid']
        }

        auth            = authorize(url=payment_endpoint,data=data)
        response        = json.loads(auth.text)

        user_data = {
            'resultCode': response['resultCode'],
            'resultText': response['resultText'],
            'user_status': response['user']['status'],
            'userIp': response['user']['userIp'],
            'msisdn': response['user']['msisdn'],
            'operatorCode': response['user']['operatorCode'],
            'uid': response['user']['uid'],
        }

        return render(request, self.template, user_data)

class AuthorizeItem(View):
    def get(self, request):
        data = {
            'command': 'wapAuthorize',
            'username': 'TESTUSER',
            'password': 'SeCrEt',
            'serviceCode': 'VAS0001',
            'price': '100',
            'uid': request.GET['uid'],
            'callbackUrl': '%s/result' % app_endpoint
        }

        auth            = authorize(url=payment_endpoint,data=data)
        response        = json.loads(auth.text)
        redirect_url    = response['redirectURL']

        return HttpResponseRedirect(redirect_url)

