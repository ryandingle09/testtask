import json
import requests

class Authorize:
    def __call__(self,url,data):
        a = requests.post(url=url, data=data)
        return a

authorize = Authorize()