Note:

Please use python3 for running payment module test task.

Guide:

-unzip testtask.zip

Files:
payment -folder
payment.sh
readme.md
start.bat
start.sh
testtask.jar

after uzipping files.

You may run on command line/Linux Terminal the ff: Commands
-make sure to be root user to avoid un necessary errors.
-navigate to extracted folder "testtask" and perform below cmd

-start test payment server app type:
./start.sh

-start running the payment simulation app tyle:
./payment.sh

Done:
Open Manually Browser and type app url:
http://127.0.0.1:8000