#!/bin/bash

virtualenv env --python=python3
source env/bin/activate
cd payment
pip install -r requirement.txt
python manage.py runserver